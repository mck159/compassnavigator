package pl.maciek.apps;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.text.InputType;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class CompassActivity extends Activity implements LocationListener, SensorEventListener{

    private static final float IMAGE_ROTATION_TOLERANCE = 7;

    private SensorManager sensorManager;
    private LocationManager locationManager;

    private Sensor sensorAccel, sensorMagnet;

    private TextView textCoordsStatus;
    private ImageView imageCompass, imageNavigator;

    private Double destLatitude = null, destLongitude = null;
    private float[] lastAccel = new float[3], lastMagnet = new float[3];
    private Float compassAzimuth = null;
    private Float navigatorOffset = null;

    private Float imageCompassAngle = null;
    private Float imageNavigatorAngle = null;

    @Override
    public void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
        sensorManager.unregisterListener(this, sensorAccel);
        sensorManager.unregisterListener(this, sensorMagnet);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!locationManager.isProviderEnabled(locationManager.GPS_PROVIDER)) {
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
            alertBuilder.setMessage(R.string.turn_on_gps_alert);
            alertBuilder.setNegativeButton(R.string.cancel_button, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
            alertBuilder.setPositiveButton(R.string.ok_button, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent gpsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(gpsIntent);
                }
            });

            alertBuilder.show();
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 400, 1, this);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_UI);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compass);

        imageCompass = (ImageView) findViewById(R.id.imageCompass);
        imageNavigator = (ImageView) findViewById(R.id.imageNavigator);
        textCoordsStatus = (TextView) findViewById(R.id.textViewCoordsStatus);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensorAccel = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorMagnet = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
    }

    public void btnLatClick(View v) {
        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
        if (destLatitude != null) {
            input.setText(destLatitude.toString());
        }
        alertBuilder.setTitle(R.string.latitude);
        alertBuilder.setView(input);
        alertBuilder.setPositiveButton(R.string.ok_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                try {
                    double answer = Double.parseDouble(input.getText().toString());
                    if (!(answer >= -90 && answer <= 90)) {
                        throw new NumberFormatException();
                    } else {
                        destLatitude = answer;

                    }
                } catch (NumberFormatException e) {
                    destLatitude = null;
                    Toast.makeText(getApplicationContext(), getString(R.string.range_restriction, -90, 90), Toast.LENGTH_SHORT).show();
                } finally {
                    updateCoordsStatus();
                }
            }
        });
        alertBuilder.show();

    }

    public void btnLongClick(View v) {
        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
        alertBuilder.setTitle(R.string.longitude);
        if (destLongitude != null) {
            input.setText(destLongitude.toString());
        }
        alertBuilder.setView(input);
        alertBuilder.setPositiveButton(R.string.ok_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                boolean invalidFormat = false;
                try {
                    double answer = Double.parseDouble(input.getText().toString());
                    if (!(answer >= -180 && answer <= 180)) {
                        throw new NumberFormatException();
                    } else {
                        destLongitude = answer;
                    }
                } catch (NumberFormatException e) {
                    destLongitude = null;
                    Toast.makeText(getApplicationContext(), getString(R.string.range_restriction, -180, 180), Toast.LENGTH_SHORT).show();
                } finally {
                    updateCoordsStatus();
                }
            }
        });
        alertBuilder.show();
    }


    @Override
    public void onLocationChanged(Location location) {
        if (destLatitude != null && destLongitude != null) {
            Location locationTarget = new Location((String) null);
            locationTarget.setLongitude(destLongitude);
            locationTarget.setLatitude(destLatitude);
            navigatorOffset = location.bearingTo(locationTarget);
            updateNavigator();
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        switch (sensorEvent.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                System.arraycopy(sensorEvent.values, 0, lastAccel, 0, sensorEvent.values.length);
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                System.arraycopy(sensorEvent.values, 0, lastMagnet, 0, sensorEvent.values.length);
                break;
        }
        float[] mR = new float[9], mI = new float[9], mValues = new float[3];
        if (sensorManager.getRotationMatrix(mR, mI, lastAccel, lastMagnet)) {
            sensorManager.getOrientation(mR, mValues);

            compassAzimuth = (float) -Math.toDegrees(mValues[0]);
        }
        updateCompass();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    public void updateCoordsStatus() {
        if (destLongitude != null && destLatitude != null) {
            textCoordsStatus.setText(getString(R.string.coords_info, destLatitude, destLongitude));
        } else {
            textCoordsStatus.setText(R.string.no_coords_set);
            imageNavigator.setRotation(0);
        }

    }

    public void updateCompass() {
        if (compassAzimuth != null) {
            if (imageCompassAngle == null) {
                imageCompassAngle = compassAzimuth;
            }
            /* Smooth rotation */
            if(Math.abs(imageCompassAngle - compassAzimuth) > IMAGE_ROTATION_TOLERANCE) {
                rotateWithAnimation(imageCompass, imageCompassAngle, compassAzimuth, 400);
                imageCompassAngle = compassAzimuth;
            }
        }
        updateNavigator();
    }

    public void updateNavigator() {
        if (compassAzimuth != null && navigatorOffset != null) {
            imageNavigator.setRotation(compassAzimuth + navigatorOffset);
            if(imageNavigatorAngle == null) {
                imageNavigatorAngle = navigatorOffset;
            }
            /* Smooth rotation */
            if(Math.abs(imageNavigatorAngle - navigatorOffset) > IMAGE_ROTATION_TOLERANCE) {
                rotateWithAnimation(imageNavigator, imageNavigatorAngle, navigatorOffset, 400);
                imageNavigatorAngle = navigatorOffset;
            }
        }
    }

    public void rotateWithAnimation(View subject, float fromAngle, float toAngle, int duration) {
        RotateAnimation animation = new RotateAnimation(fromAngle, toAngle, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setDuration(duration);
        animation.setInterpolator(new LinearInterpolator());
        animation.setFillAfter(true);
        subject.startAnimation(animation);
    }
}
